Bulk Domain Checker
============================


REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.

CONFIGURATION
-------------

### Database

Add a file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=bulkdomainchecker',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

### Global System Config

Make changes to `config/params.php`
