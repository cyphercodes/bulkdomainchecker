<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Bulkdomain;

class BulkAllController extends Controller {

    public function actionIndex() {
        if ($bulkdomains = Bulkdomain::find()->where("status = :status", [':status' => Bulkdomain::STATUS_NEW])->all()) {
            foreach ($bulkdomains as $bulkdomain) {
                $bulkdomain->status = Bulkdomain::STATUS_PROCESSING;
                $bulkdomain->save();
            }
            foreach ($bulkdomains as $bulkdomain) {
                $bulkdomain->processDomains();
            }
        }
    }

}
