<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Bulkdomain;

class BulkController extends Controller {

    public $enableCsrfValidation = false;

    public function actionIndex($id) {
        if ($bulkdomain = Bulkdomain::findOne($id)) {
            $bulkdomain->processDomains();
        }
    }

}
