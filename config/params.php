<?php

return [
    'adminEmail' => 'sam@custard.co.uk',
    'users' => [
        '100' => [
            'id' => '100',
            'username' => 'sam@custard.co.uk',
            'password' => 'Custardp1e',
            'authKey' => '100keyCustard1p13',
            'accessToken' => '100tokenCustard1p13',
        ]
    ],
    'proxies' => [
        //FIRST 10 PRIVATE
        '5.231.237.6:4304',
        '5.231.237.101:4399',
        '5.231.237.112:4410',
        '94.249.224.95:1222',
        '94.249.224.195:1322',
        '94.249.224.13:1140',
        '195.162.4.234:3362',
        '195.162.4.254:3382',
        '195.162.4.75:3203',
        '185.13.156.151:2279',
        //NEXT 50 SHARED
        '5.231.237.22:4320',
        '5.231.237.109:4407',
        '5.231.237.172:4470',
        '5.230.31.85:2213',
        '5.230.31.42:2170',
        '5.230.31.95:2223',
        '5.230.31.234:2362',
        '5.230.31.49:2177',
        '5.230.31.218:2346',
        '5.230.31.189:2317',
        '5.230.31.24:2152',
        '5.230.31.6:2134',
        '5.230.154.240:8418',
        '5.230.154.28:8206',
        '5.230.154.181:8359',
        '5.230.154.243:8421',
        '5.230.154.175:8353',
        '5.230.154.63:8241',
        '5.230.154.71:8249',
        '5.230.153.163:8291',
        '5.230.153.127:8255',
        '5.230.153.87:8215',
        '5.230.153.88:8216',
        '5.230.153.149:8277',
        '5.230.153.234:8362',
        '5.230.153.17:8145',
        '5.230.153.249:8377',
        '5.230.153.25:8153',
        '5.230.153.19:8147',
        '5.230.153.247:8375',
        '195.162.4.238:3366',
        '195.162.4.135:3263',
        '195.162.4.169:3297',
        '185.13.156.66:2194',
        '185.13.156.213:2341',
        '185.13.156.115:2243',
        '185.13.156.134:2262',
        '185.13.156.194:2322',
        '185.13.156.147:2275',
        '185.13.156.185:2313',
        '185.13.156.215:2343',
        '185.13.156.187:2315',
        '185.13.156.92:2220',
        '185.13.156.245:2373',
        '185.13.156.3:2131',
        '185.13.156.109:2237',
        '185.13.156.98:2226',
        '185.13.156.211:2339',
        '178.18.151.146:1273',
        '178.18.151.222:1349',
    ],
    'user-agents' => [
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
        'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.6 (Change: )',
        'Mozilla/5.0 (Windows NT 6.1; rv:27.3) Gecko/20130101 Firefox/27.3',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2117.157 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A',
        'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
        'Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14',
    ],
    'mozApis' => [
//        ["accessId" => "mozscape-5e9a09c6e5", "secretKey" => "3c77611f5a914314f1092ca21c1dd85e"], // BANNED
//        ["accessId" => "mozscape-68212eeeb0", "secretKey" => "4d94407cf580ae2d74958ea68b5dbbe4"], // BANNED
//        ["accessId" => "mozscape-55d3f60181", "secretKey" => "d52f85595d8b90a8e75536d41416f1e4"], // BANNED
//        ["accessId" => "mozscape-bc028f3c0", "secretKey" => "f218b7b73ca636f92aa49f87e57013be "],
//        ["accessId" => "mozscape-75ef36889", "secretKey" => "4978946dae56540ed7ed7dd6bfc6b781"],  // BANNED
//        ["accessId" => "mozscape-1546d76c7a", "secretKey" => "2be0d4f3f89c4e9bacb036952ce58d4c"], // BANNED
//        ["accessId" => "mozscape-15ad485613", "secretKey" => "4ca8166b6245ea2ebfa0f39ede86f13e"], // BANNED
//        ["accessId" => "mozscape-c093713f1f", "secretKey" => "c1ed2fcc82528a67659dae816e72bfb8"], // BANNED
//        ["accessId" => "mozscape-d4b56d6efa", "secretKey" => "a0209500e8214671a34e0136dc0fc184"], // BANNED
//        ["accessId" => "mozscape-41f9c20ef8", "secretKey" => "655e7ee454d81f103b5f9356ce3aec30"], // BANNED
//        ["accessId" => "mozscape-1f62faad38", "secretKey" => "97f68f2c0401aad42987de2256c3fffa"], // BANNED
//        ["accessId" => "mozscape-49a98595b1", "secretKey" => "aac1113f190d361f6db041461b95464f"], // BANNED
//        ["accessId" => "mozscape-e806be8621", "secretKey" => "964c1aa2a61b6ee606f7332048cfe956"], // BANNED
//        ["accessId" => "mozscape-d936420ab7", "secretKey" => "d97802a6251d8ced6a6e87ed1ca44d64"], // BANNED
//        ["accessId" => "mozscape-3c8d5dd7e2", "secretKey" => "2e5283e34e609f6c698af27f6ca2cbed"], // BANNED
//        ["accessId" => "mozscape-1ea3e9ed82", "secretKey" => "9492c0b29573912ef648c90d766f020c"], // BANNED
//        ["accessId" => "mozscape-23aa215af5", "secretKey" => "130b03cca84d1f21b4141ce96e5c9c6b"], // BANNED
//        ["accessId" => "mozscape-6eba4f87b5", "secretKey" => "3578165091c03cec564a4a2c3559b840"], // BANNED
//        ["accessId" => "mozscape-718e0d3ffd", "secretKey" => "d15649893042e849ef5ac6103d2730f8"], // BANNED
        ["accessId" => "mozscape-45020c83fd", "secretKey" => "1dda8ecde19e5f336d3bc4a4baa47f75"],
        ["accessId" => "mozscape-5ffbc8e697", "secretKey" => "b8458eb4beb81018f551d8e1b610a436"],
        ["accessId" => "mozscape-996403535a", "secretKey" => "f050882ee5c156ac7f82b37df91d1492"],
        ["accessId" => "mozscape-f10d973608", "secretKey" => "bf7a778568b716dd8d4f9f7f649b4710"],
        ["accessId" => "mozscape-753a36a700", "secretKey" => "d2c28db6cc34e7591585301d21c0190b"],
        ["accessId" => "mozscape-df88199601", "secretKey" => "c733df4174303ca90cd33212e2704ce5"],
//        ["accessId" => "mozscape-668efffdf9", "secretKey" => "4e5fcdfbf5f6c93b35dbb7d261bf99 "],
        ["accessId" => "mozscape-737b307a00", "secretKey" => "b1395f2e4c5ce63af584ab0b22a6bbca"],
        ["accessId" => "mozscape-ab23bd3fec", "secretKey" => "1890ba11009bd19d7d104b4948046510"],
        ["accessId" => "mozscape-f12ae1768c", "secretKey" => "7d7fc647211d1602c36a096f29662508"],
        ["accessId" => "mozscape-350f314546", "secretKey" => "147c720a09ae6bbe923b01b81f1bed87"],
//        ["accessId" => "mozscape-76feb1d9d2", "secretKey" => "955cb050810e790a76f334ce06323fc "],
        ["accessId" => "mozscape-924e19a1c8", "secretKey" => "cbc4a388605886af6e2588a7f9f3d6c5"],
        ["accessId" => "mozscape-bc9cc6f7ba", "secretKey" => "2e4cef5671026ec73b4aad61247ff24a"],
        ["accessId" => "mozscape-2c4e052000", "secretKey" => "3a54b4ff344bde54db3dbdb3e5a93702"],
        ["accessId" => "mozscape-3bfba2e1b6", "secretKey" => "ae6d780fbac06b945f714e244217aaa6"],
        ["accessId" => "mozscape-3397dfeee5", "secretKey" => "6e9dd5bf0ea646f2652a0acb72e23abf"],
        ["accessId" => "mozscape-8125d8f58b", "secretKey" => "ba2a80bc0c63c6e2fbf772b6201c7b69"],
        ["accessId" => "mozscape-f6f4870268", "secretKey" => "2d9b9ffb164603ebffe77f513fe809a3"],
        ["accessId" => "mozscape-3397524c3c", "secretKey" => "ac3a35f24b23920702eb57bc3efa21d8"],
        ["accessId" => "mozscape-151f87d474", "secretKey" => "406000958032268ff72b6b2095bdf355"],
        ["accessId" => "mozscape-48b9f52dbf", "secretKey" => "9d032b507fccca067e34509294125158"],
    ],
    'googleScraper' => [
        'tryNTimes' => 5, // how many times to try scraping each domain with random proxies and user-agents
        'sleepMin' => 2, // Minimum seconds to sleep
        'sleepMax' => 10, //Maximum seconds to sleep
        'deepNTimes' => 3, // Start DEEP sleep attempts after how many failures?: should be less than `tryNTimes`
        // If scraping failed for `deepNTimes`, the remaining attempts will be done with the below sleep values:
        'deepSleepMin' => 60, // Minimum seconds to sleep in DEEP mode attempts
        'deepSleepMax' => 120, // Maximum seconds to sleep in DEEP mode attempts
        'enableGoogleIndex' => false, // Enable or disable Google Search Index
    ]
];
