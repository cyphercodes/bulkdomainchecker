<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\DomainsForm;
use Goutte\Client;
use app\models\Bulkdomain;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use app\models\MozBulk;

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'login'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            $this->redirect(['site/login']);
        }
        $model = new DomainsForm();
        if ($model->load(Yii::$app->request->post()) && $model->start()) {
            Yii::$app->session->setFlash('domainsFormSubmitted');
            return $this->refresh();
        }

        $bulkDomains = new ActiveDataProvider([
            'query' => Bulkdomain::find(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);
        return $this->render('index', [
                    'model' => $model,
                    'bulkDomains' => $bulkDomains,
        ]);
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

//    public function actionTest() {
//        $proxies = Yii::$app->params['proxies'];
//        $user_agents = Yii::$app->params['user-agents'];
//        $currProxy = $proxies[mt_rand(0, count($proxies) - 1)];
//        $client = new \app\models\CustomGoutte();
//        $crawler = $client->request('GET', 'https://google.com/search?q=site:tralexa.com&tbm=nws', [
//            'config' => [
//                'verify' => false,
//                'proxy' => $currProxy, // set random proxy from list
//                'headers' => [
//                    'User-Agent' => $user_agents[mt_rand(0, count($user_agents) - 1)], // set random user-agent
//                    'Referer' => 'https://www.google.com',
//                ],
//            ]
//        ]);
//        $result = $crawler->filter('div#resultStats')->each(function ($node) {
//            $subs = explode("results", $node->text());
//            if (isset($subs[0])) {
//                echo intval(preg_replace('/[^0-9]+/', '', $subs[0]));
//            }
//        });
//        if (count($result) > 0) {
//            echo $result[0];
//        }
//        echo $crawler->html();
//    }
}
