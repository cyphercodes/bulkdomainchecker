<?php

use yii\db\Migration;

class m160409_140823_initial extends Migration {

    public function up() {
        $this->createTable('bulkdomain', array(
            'id' => 'pk',
            'status' => 'smallint(6) DEFAULT NULL',
            'updated_at' => 'int(11) NOT NULL',
            'created_at' => 'int(11) NOT NULL',
                ), '');

        $this->createTable('domain', array(
            'id' => 'pk',
            'url' => 'varchar(255) NOT NULL',
            'google_index' => 'int(11) DEFAULT NULL',
            'google_news_index' => 'int(11) DEFAULT NULL',
            'moz_domain_authority' => 'float DEFAULT NULL',
            'moz_page_authority' => 'float DEFAULT NULL',
            'moz_backlinks' => 'int(11) DEFAULT NULL',
            'moz_rank' => 'float DEFAULT NULL',
            'status' => 'smallint(6) NOT NULL',
            'created_at' => 'int(11) NOT NULL',
            'updated_at' => 'int(11) NOT NULL',
            'bulkdomain_id' => 'int(11) NOT NULL',
                ), '');

        $this->createIndex('idx_bulkdomain_id', 'domain', 'bulkdomain_id', FALSE);

        $this->createTable('session', array(
            'id' => 'char(40) NOT NULL',
            'expire' => 'int(11) DEFAULT NULL',
            'data' => 'blob DEFAULT NULL',
                ), '');

        $this->addPrimaryKey('pk_session', 'session', 'id');

        $this->addForeignKey('fk_domain_bulkdomain_bulkdomain_id', 'domain', 'bulkdomain_id', 'bulkdomain', 'id', 'NO ACTION', 'NO ACTION');
    }

    public function down() {
        $this->dropForeignKey('fk_domain_bulkdomain_bulkdomain_id', 'domain');

        $this->dropTable('bulkdomain');
        $this->dropTable('domain');
        $this->dropTable('session');
    }

}
