<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use Swift_Attachment;
use yii\helpers\Html;

/**
 * This is the model class for table "bulkdomain".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $updated_at
 * @property integer $created_at
 *
 * @property Domain[] $domains
 */
class Bulkdomain extends ActiveRecord {

    const STATUS_COMPLETE = 20;
    const STATUS_PROCESSING = 10;
    const STATUS_NEW = 0;

    public $progress;

    public static function tableName(
    ) {
        return 'bulkdomain';
    }

    public function behaviors() {

        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules() {
        return [
            [['status', 'updated_at',
            'created_at'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_PROCESSING, self::STATUS_NEW, self::STATUS_COMPLETE]],
            ['status', 'default', 'value' => self::STATUS_NEW],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getDomains() {
        return $this->hasMany(Domain::className(), ['bulkdomain_id' => 'id']);
    }

    public function getCompletedDomains() {
        return $this->hasMany(Domain::className(), ['bulkdomain_id' => 'id'])->where(['status' => Domain::STATUS_COMPLETE]);
    }

    public function afterFind() {
        $this->progress = number_format((float) count($this->completedDomains) / (count($this->domains) == 0 ? 1 : count($this->domains)) * 100, 2, '.', '');
        return parent::afterFind();
    }

    public function startCommand() {
        if (DIRECTORY_SEPARATOR == '/') { //Is LINUX
            Yii::warning('php ' . Yii::$app->basePath .
                    DIRECTORY_SEPARATOR . 'yii bulk ' . $this->id . ' > ' . DIRECTORY_SEPARATOR . 'dev' . DIRECTORY_SEPARATOR . 'null &');
            exec('php ' . Yii::$app->basePath .
                    DIRECTORY_SEPARATOR . 'yii bulk ' . $this->id, $output); // FOR LINUX
//            $output = shell_exec('php ' . Yii::$app->basePath .
//                    DIRECTORY_SEPARATOR . 'yii bulk ' . $this->id);
            Yii::warning("OUTPUT:" . $output);
        } else {
            pclose(popen("start /B cmd /C " . Yii::$app->basePath . DIRECTORY_SEPARATOR . 'yii bulk ' . $this->id . ' >NUL 2>NUL &', "r")); //FOR WINDOWS
        }
    }

    public function processDomains() {
        foreach ($this->domains as $domain) {
            $domain->process();
        }
        $mozbulk = new MozBulk($this->domains);
        $this->status = self::STATUS_COMPLETE;
        $this->save();
        $this->sendReport();
    }

    public function status() {
        switch ($this->status) {
            case self::STATUS_COMPLETE:
                return "Complete";
            case self::STATUS_PROCESSING:
                return "Processing";
            case self::STATUS_NEW:
                return "New";
            default: return "Unknown";
        }
    }

    public function progress() {
        return $this->progress . '% (' . count($this->completedDomains) . '/' . count($this->domains) . ')';
    }

    public function sendReport() {
//        $bulk = Bulkdomain::findOne(16);
        $fp = fopen("Report.csv", "w");
        $headers = array(
            'url',
            'google_index',
            'google_news_index',
            'moz_domain_authority',
            'moz_page_authority',
            'moz_backlinks',
            'moz_rank'
        );
        $titles = array(
            'domain',
            'google_index',
            'google_news_index',
            'moz_domain_authority',
            'moz_page_authority',
            'moz_backlinks',
            'moz_rank'
        );
        fputcsv($fp, $titles);
        foreach ($this->domains as $domain) {
            $row = array();
            foreach ($headers as $head) {
                $row[] = Html::getAttributeValue($domain, $head);
            }
            fputcsv($fp, $row);
        }
        rewind($fp);
//        Yii::$app->response->sendFile(stream_get_meta_data($fp)['uri'], 'Export.csv')->send(); //To Export
        $message = Yii::$app->mailer->compose()
                ->setFrom('reports@tool.webain.co.uk')
                ->setTo(Yii::$app->params['adminEmail'])
                ->setSubject('Bulk Domain Report - ' . date('d-m-Y') . " - ID: " . $this->id)
                ->setHtmlBody('Please find attached file which has the report for bulk process ID : ' . $this->id
                . "<br>Number of domains: " . count($this->domains)
                . "<br>Started at: " . date("Y-m-d H:i", $this->created_at)
                . "<br>Ended at: " . date("Y-m-d H:i", $this->updated_at));

        $message->attach(stream_get_meta_data($fp)['uri']);
        $message->send();
        fclose($fp);
        unlink("Report.csv");
    }

}
