<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\CustomGoutte;

/**
 * This is the model class for table "domain".
 *
 * @property string $id
 * @property string $url
 * @property integer $google_index
 * @property integer $google_news_index
 * @property double $moz_domain_authority
 * @property double $moz_page_authority
 * @property integer $moz_backlinks
 * @property double $moz_rank
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $bulkdomain_id
 *
 * @property Bulkdomain $bulkdomain
 */
class Domain extends ActiveRecord {

    const STATUS_COMPLETE = 20;
    const STATUS_PROCESSING = 10;
    const STATUS_FAILED = 0;

    public static function tableName() {
        return 'domain';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules() {
        return [
            [['url', 'bulkdomain_id'], 'required'],
            [['google_index', 'google_news_index', 'moz_backlinks', 'status', 'created_at', 'updated_at', 'bulkdomain_id'], 'integer'],
            [['moz_domain_authority', 'moz_page_authority', 'moz_rank'], 'number'],
            [['url'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_PROCESSING],
            ['status', 'in', 'range' => [self::STATUS_PROCESSING, self::STATUS_FAILED, self::STATUS_COMPLETE]],
            [['bulkdomain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bulkdomain::className(), 'targetAttribute' => ['bulkdomain_id' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'google_index' => 'Google Index',
            'google_news_index' => 'Google News Index',
            'moz_domain_authority' => 'Moz Domain Authority',
            'moz_page_authority' => 'Moz Page Authority',
            'moz_backlinks' => 'Moz Backlinks',
            'moz_rank' => 'Moz Rank',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'bulkdomain_id' => 'Bulkdomain ID',
        ];
    }

    public function getBulkdomain() {
        return $this->hasOne(Bulkdomain::className(), ['id' => 'bulkdomain_id']);
    }

    public function beforeValidate() {
//        if ((substr($this->host(), -1) == '.') || !checkdnsrr($this->host(), 'A')) {  // Checks if domain is active
//            $this->valid = self::STATUS_FAILED;
//        } else {
//            $this->valid = self::STATUS_VALID;
//        }
        return parent::beforeValidate();
    }

    public function host() {
        if (filter_var($this->url, FILTER_VALIDATE_URL) !== false) {
            $parse = parse_url($this->url);
            return $url = $parse['host'];
        }
        return $this->url;
    }

    public function process() {
        if ($this->fetchIndexes()) {
            $this->status = self::STATUS_COMPLETE;
        } else {
            $this->status = self::STATUS_FAILED;
        }
        $this->save();
    }

    public function fetchIndexes() {
        if (Yii::$app->params['googleScraper']['enableGoogleIndex']) {
            $this->fetchGoogleIndex();
        }
        $this->fetchGoogleNewsIndex();
        return true;
    }

    public function randomGoogleLink() {
        $googlelinks = [
            'https://www.google.com',
            'https://www.google.co.uk',
            'https://www.google.ca',
        ];
        return $googlelinks[mt_rand(0, count($googlelinks) - 1)];
    }

    public function fetchGoogleIndex() {
        $this->google_index = $this->scrapIndexFromGoogle($this->randomGoogleLink() . '/search?q=site:' . $this->url);
    }

    public function fetchGoogleNewsIndex() {
        $this->google_news_index = $this->scrapIndexFromGoogle($this->randomGoogleLink() . '/search?q=site:' . $this->url . '&tbm=nws');
    }

    public function scrapIndexFromGoogle($url) {
        $proxies = Yii::$app->params['proxies'];
        $user_agents = Yii::$app->params['user-agents'];
        $x = 0;
        while ($x < Yii::$app->params['googleScraper']['tryNTimes']) { //Try x times using random proxies and random user-agents
            $currProxy = $proxies[mt_rand(0, count($proxies) - 1)];
            if ($x <= Yii::$app->params['googleScraper']['deepNTimes']) {
                sleep(mt_rand(Yii::$app->params['googleScraper']['sleepMin'], Yii::$app->params['googleScraper']['sleepMax'])); //sleep between each attempt
            } else {
                sleep(mt_rand(Yii::$app->params['googleScraper']['deepSleepMin'], Yii::$app->params['googleScraper']['deepSleepMax'])); //Deep sleep if failed `deepNTimes` attempts and more
            }
            $client = new CustomGoutte();
            $crawler = $client->request('GET', $url, [
                'config' => [
                    'verify' => false,
                    'proxy' => $currProxy, // set random proxy from list
                    'headers' => [
                        'User-Agent' => $user_agents[mt_rand(0, count($user_agents) - 1)], // set random user-agent
                        'Referer' => 'https://www.google.com',
                    ],
                ]
            ]);

            $result = $crawler->filter('div#resultStats')->each(function ($node) {
                $subs = explode("results", $node->text());
                if (isset($subs[0])) {
                    return intval(preg_replace('/[^0-9]+/', '', $subs[0]));
                }
            });
            if (count($result) > 0) {
                return $result[0]; // if rank is found, return it
            }
            Yii::warning("This proxy may be blocked from Google: " . $currProxy);
            $x++;
        }
        return -1; //If tried x times, without getting correct results, set indexes to -1
    }

    public function setMozInfo($moz_domain_authority, $moz_page_authority, $moz_backlinks, $moz_rank) {
        $this->moz_domain_authority = $moz_domain_authority;
        $this->moz_page_authority = $moz_page_authority;
        $this->moz_backlinks = $moz_backlinks;
        $this->moz_rank = $moz_rank;
        $this->save();
    }

}
