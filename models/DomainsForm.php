<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Domain;

/**
 * ContactForm is the model behind the contact form.
 */
class DomainsForm extends Model {

    public $text;
    public $email;
    public $domainsArr;
    public $bulkdomainid;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['text'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['email', 'default', 'value' => Yii::$app->params['adminEmail']],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'text' => 'List of Domains',
        ];
    }

    public function beforeValidate() {
        return parent::beforeValidate();
    }

    public function start() {
        if ($this->validate()) {
            $this->text = trim($this->text);
            $this->domainsArr = explode("\n", $this->text);
            if ($bd = $this->saveBulkDomain()) {
                //process from console...
//                $bd->startCommand();
            }
//            var_dump($this->domainsArr); die();
//            Yii::$app->mailer->compose()
//                    ->setTo($this->email)
////                    ->setFrom([$this->email => $this->name])
//                    ->setSubject("New Report Generated!")
//                    ->setTextBody(print_r($this->domainsArr, true))
//                    ->send();

            return true;
        }
        return false;
    }

    public function saveDomain($domain) {
        $domain = trim($domain);
        $model = new Domain();
        $model->url = $domain;
        $model->bulkdomain_id = $this->bulkdomainid;
        $model->save();
    }

    public function saveDomains() {
        if (count($this->domainsArr) < 1 || !isset($this->bulkdomainid)) {
            return false;
        }
        foreach ($this->domainsArr as $domain) {
            $this->saveDomain($domain);
        }
        return true;
    }

    public function saveBulkDomain() {
        $bulkdomain = new Bulkdomain();
        if ($bulkdomain->save()) {
            $this->bulkdomainid = $bulkdomain->getPrimaryKey();
            if ($this->saveDomains()) {
                return $bulkdomain;
            }
        }
        return false;
    }

}
