<?php

namespace app\models;

use Yii;
use app\models\Moz;

class MozBulk {

    public $apis;
    public $domains; //Array of Domain Objects

    public function __construct($domains) {
        $this->apis = Yii::$app->params['mozApis'];
        $this->domains = $domains;
        $this->process();
    }

    public function process() {
        $x = 0;
        foreach ($this->domains as $domain) {
            do {
                $api = new Moz($this->apis[$x]['accessId'], $this->apis[$x]['secretKey']);
                $url_metrics = json_decode($api->urlMetrics($domain->url, 2048 + 16384 + 34359738368 + 68719476736));
                if (isset($url_metrics->error_message)) {
                    Yii::warning("Invalid MOZ API Credentials for: " . $this->apis[$x]['accessId']);
                }
                $x++;
                if ($x == count($this->apis)) {
                    $x = 0;
                    sleep(count($this->apis) + 5);
                }
            } while (isset($url_metrics->error_message));
            $domain->setMozInfo($url_metrics->{"pda"}, $url_metrics->{"upa"}, $url_metrics->{"uid"}, $url_metrics->{"umrp"});
        }
    }

}
