<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>


        <nav class="teal">
            <div class="nav-wrapper container">
                <a href="<?= Url::toRoute(['/site/index']); ?>" class="brand-logo">Bulk Domain Checker</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="<?= Url::toRoute(['/site/index']); ?>">Home</a></li>
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <li><a href="<?= Url::toRoute(['/site/login']); ?>">Login</a></li>
                    <?php } else { ?>
                        <li><a href="<?= Url::toRoute(['/site/logout']); ?>">logout</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
        <main>
            <div class="container">
                <?= $content ?>
            </div>
        </main>

        <footer class="page-footer teal">
            <div class="footer-copyright teal">
                <div class="container">
                    © <?= date('Y') ?> Bulk Domain Checker
                    <span class="grey-text text-lighten-4 right">Created by CypherCodes</span>
                </div>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
