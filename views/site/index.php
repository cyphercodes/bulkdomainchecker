<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

$this->title = 'Bulk Domain Checker';
?>
<?php if (Yii::$app->session->hasFlash('domainsFormSubmitted')) { ?>
    <div class="card-panel green">
        <span class="white-text">
            Domains submitted. You will receive an E-mail once they are processed.
        </span>
    </div>
<?php } ?>
<div class="card-panel white">
    <div class="card-content">
        <?php $form = ActiveForm::begin(['id' => 'domain-form']); ?>

        <?= $form->field($model, 'text')->textArea(['rows' => 6, 'class' => 'materialize-textarea']) ?>

        <div class="center">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary waves-effect waves-light', 'name' => 'contact-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="card-panel white">
    <div class="card-content">
        <?php
        echo GridView::widget([
            'dataProvider' => $bulkDomains,
            'tableOptions' => ['class' => 'highlight centered responsive-table'],
            'columns' => [
                'id',
                [
                    'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                    'attribute' => 'status',
                    'value' => function ($data) {
                        return $data->status();
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:Y-m-d H:i']
                ],
                [
                    'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
                    'attribute' => 'progress',
                    'value' => function ($data) {
                        return $data->progress();
                    },
                ],
            ],
        ]);
        ?>
    </div>
</div>