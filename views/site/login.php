<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Login';
?>
<div class="card-panel white">
    <h4 class="center">Login</h4>

    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
    ]);
    ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe', ['template' => "{input} {label}",])->checkbox([], false) ?>


    <div class="center">
        <?= Html::submitButton('Login', ['class' => 'waves-effect waves-light btn', 'name' => 'login-button']) ?>
    </div>

    <?php ActiveForm ::end(); ?>

</div>
